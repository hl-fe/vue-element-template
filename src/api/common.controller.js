import http from '@/utils/Httprequest';

export default {
	/**
	 * 列表获取
	 * @param {*} param0
	 */
	tableFetchData ({url,method ='post',data}) {
		return http({
			url,
			method,
			data
		});
	},
	/**
	 * 文件上传
	 * @param {*} data
	 * @param {*} callback
	 */
	fileupload (data,callback) {
		return http({
			url : '/upload',
			data,
			headers : {
				'Content-Type' : 'multipart/form-data'
			},
			onUploadProgress : ProgressEvent => {
				if (callback){
					let progressPercent = ProgressEvent.loaded / ProgressEvent.total;
					callback(progressPercent<1?(progressPercent*100).toFixed(2):progressPercent*100);
				}

			}
		});
	}
};
