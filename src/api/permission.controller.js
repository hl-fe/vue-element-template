import http from '@/utils/Httprequest';

export default {
	/**
	 * 获取菜单
	 * @param {*} userId
	 */
	getMenu (userId) {
		return http({
			url : '/permission/getMenu/'+userId,
			method : 'get'
		});
	},
	/**
	 * 获取菜单包含 按钮-api
	 */
	getMenuAndBtnApi () {
		return http({
			url : '/permission/getMenuAndBtnApi',
			method : 'get'
		});
	},
	/**
	 * 用户分配角色
	 */
	assignRoles (userId,rolesIds){
		return http({
			url : '/permission/assignRoles/'+userId +'/'+rolesIds
		});
	},
	/**
	 * 角色分配权限
	 */
	assignPermission (roleId,permissionIds){
		return http({
			url : '/permission/assignPermission/'+roleId +'/'+permissionIds
		});
	},
	/**
	 * 新增权限
	 * @param {*} data
	 */
	permissionAdd (data){
		return http({
			url : '/permission',
			data
		});
	},
	/**
	 * 根据id删除
	 * @param {*} id
	 */
	permissionDelete (id) {
		return http({
			method : 'delete',
			url : '/permission/'+id
		});
	},
	/**
	 * 权限修改
	 * @param {*} data
	 */
	permissionUpdate (data) {
		return http({
			method : 'put',
			url : '/permission',
			data
		});
	},
	/**
	 * 根据角色ID查询所有的权限ID
	 * @param {*} roleId
	 */
	getPermissionIdsByRoleId (roleId){
		return http({
			method : 'get',
			url : '/permission/findPermissionIdsByRoleId/'+roleId
		});
	}


};
