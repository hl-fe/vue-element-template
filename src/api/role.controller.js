import http from '@/utils/Httprequest';

export default {
	/**
	 * 获取所有角色
	 */
	getRoles (){
		return http({
			url : '/role',
			method : 'get'
		});
	},
	/**
	 * 角色新增
	 * @param {*} data
	 */
	roleAdd (data) {
		return http({
			url : '/role',
			data
		});
	},
	/**
	 * 角色修改
	 * @param {*} data
	 */
	roleUpdate (data) {
		return http({
			method : 'put',
			url : '/role',
			data
		});
	},
	/**
	 * 根据id删除
	 * @param {*} id
	 */
	roleDelete (id) {
		return http({
			method : 'delete',
			url : '/role/'+id
		});
	}
};
