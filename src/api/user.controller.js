import http from '@/utils/Httprequest';

export default {
	/**
	 * 用户登录
	 * @param {*} data
	 */
	login (data) {
		return http({
			url : '/user/login',
			data
		});
	},
	/**
	 * 用户新增
	 * @param {*} data
	 */
	userAdd (data) {
		return http({
			url : '/user',
			data
		});
	},
	/**
	 * 用户修改
	 * @param {*} data
	 */
	userUpdate (data) {
		return http({
			method : 'put',
			url : '/user',
			data
		});
	},
	/**
	 * 根据id删除
	 * @param {*} id
	 */
	userDelete (id) {
		return http({
			method : 'delete',
			url : '/user/'+id
		});
	},
	/**
	 * 根据用户id查询角色
	 * @param {*} id
	 */
	findByIdAndRole (id){
		return http({
			method : 'get',
			url : '/user/findByIdAndRole/'+id
		});
	}
};
