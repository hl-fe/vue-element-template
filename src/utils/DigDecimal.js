import Big from 'big';
export default class DigDecimal {
	// 加
	static add (numA, numB) {
		return new Big(numA).plus(new Big(numB)).toString();
	}
	// 减
	static subtract (numA, numB) {
		return new Big(numA).minus(new Big(numB)).toString();
	}
	// 乘
	static multiply (numA, numB) {
		return new Big(numA).times(new Big(numB)).toString();
	}
	// 除
	static divide (numA, numB) {
		return new Big(numA).div(new Big(numB)).toString();
	}
	// 分转元
	static fenToYuan (fen) {
		return DigDecimal.divide(fen, 100);
	}
	// 元转分
	static yuanToFen (yuan) {
		return DigDecimal.multiply(yuan, 100);
	}
}
