// 所有的的常量值
export default class Const {
	static USER = 'USER' // 用户信息
	static TREE_LIST = 'TREE_LIST' // 菜单树
	static RESET_TREE_LIST = 'RESET_TREE_LIST' // 清空菜单树
	static PERMISSIONS = 'PERMISSIONS' // 权限
	static COLLAPSE = 'COLLAPSE' // 菜单展开收起
	static AXIOS_ENQUEUE = 'AXIOS_ENQUEUE' // 请求取消标记
	static AXIOS_CLEAR_QUEUE = 'AXIOS_CLEAR_QUEUE' // 取消请求
}
