import Permissions from '@/components/Permissions';
import TableCom from '@/components/TableCom';
import SearchHeader from '@/components/SearchHeader';
import DialogForm from '@/components/DialogForm';
import CommonUpload from '@/components/CommonUpload';


function createDialogForm (Vue) {
	const DialogFormConstructor = Vue.extend(DialogForm);
	const instance = new DialogFormConstructor();
	instance.$mount(document.createElement('div'));
	document.body.appendChild(instance.$el);
	Vue.prototype.$dialogForm = instance;
}
export default {
	install (Vue) {
		Vue.component('Permissions', Permissions);
		Vue.component('TableCom', TableCom);
		Vue.component('SearchHeader', SearchHeader);
		Vue.component('CommonUpload', CommonUpload);
		createDialogForm(Vue);
	}
};
