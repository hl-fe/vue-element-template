import Vue from 'vue';
import Vuex from 'vuex';
import User from './modules/user';
import Menu from './modules/menu';
import Permissions from './modules/permissions';
import AxiosCancel from './modules/axiosCancel';
Vue.use(Vuex);

export default new Vuex.Store({
	modules : {
		User,
		Menu,
		Permissions,
		AxiosCancel
	},
	strict : process.env.NODE_ENV !== 'production'
});
