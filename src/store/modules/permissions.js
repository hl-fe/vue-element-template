import CONST from '@/utils/Const';
import Storage from '@/utils/Storage';
export default {
	namespaced : true,
	state : {
		roles : Storage.get(CONST.PERMISSIONS,1) || []
	},
	getters : {
		getRoles : state => {
			return state.roles;
		}
	},
	mutations : {
		[CONST.PERMISSIONS] (state, data) {
			state.roles = data;
			Storage.set(CONST.PERMISSIONS, data,1);
		}
	},
	actions : {
		permissionsAction ({ commit }, item) {
			commit(CONST.PERMISSIONS, item);
		}
	}
};
