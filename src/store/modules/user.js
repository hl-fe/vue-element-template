import CONST from '@/utils/Const';
import Storage from '@/utils/Storage';
export default {
	namespaced : true,
	state : {
		user : Storage.get(CONST.USER,1) || {}
	},
	getters : {
		user : state => {
			return state.user;
		}
	},
	mutations : {
		[CONST.USER] (state, data) {
			state.user = data;
			Storage.set(CONST.USER, data,1);
		}
	},
	actions : {
		userAction ({ commit }, item) {
			commit(CONST.USER, item);
		}
	}
};
