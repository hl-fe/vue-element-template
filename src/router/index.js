import Vue from 'vue';
import Router from 'vue-router';

const Layout = () => import(/* webpackChunkName:'Layout' */ '@/Layout');
const Home = () => import(/* webpackChunkName:'Home' */ '@/views/Home');
const permission = () => import(/* webpackChunkName:'permission' */ '@/views/Permission');
const Role = () => import(/* webpackChunkName:'Role' */ '@/views/Role');
const User = () => import(/* webpackChunkName:'User' */ '@/views/User');
const Login = () => import(/* webpackChunkName:'Login' */ '@/views/Login');

Vue.use(Router);
// 去除路由重复跳转警告
const originalPush = Router.prototype.push;
Router.prototype.push = function push (location) {
	return originalPush.call(this, location).catch(err => err);
};
// 动态路由表
export const asyncRoutes = [
	{
		path : '/home',
		component : Layout,
		meta : {
			title : '首页',
			icon : '',
			roles : []
		},
		children : [
			{
				path : '/',
				component : Home,
				meta : {
					title : '首页',
					icon : '',
					roles : []
				}
			}
		]
	},
	{
		path : '/system',
		component : Layout,
		meta : {
			title : '系统管理',
			icon : '',
			roles : []
		},
		children : [
			{
				path : 'permission',
				component : permission,
				meta : {
					title : '权限管理',
					icon : '',
					roles : []
				}
			},
			{
				path : 'role',
				component : Role,
				meta : {
					title : '角色管理',
					icon : '',
					roles : []
				}
			},
			{
				path : 'user',
				component : User,
				meta : {
					title : '用户管理',
					icon : '',
					roles : []
				}
			}
		]
	}
];

let router = new Router({
	base : process.env.BASE_URL,
	routes : [
		{
			path : '/login',
			component : Login,
			hidden : true, // 导航菜单忽略该项
			meta : {
				title : '登录'
			}
		}
	]
});

export default router;
