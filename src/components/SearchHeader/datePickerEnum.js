export default {
	year : 'year',
	month : 'month',
	date : 'date',
	dates : 'dates',
	week : 'week',
	datetime : 'datetime',
	datetimerange : 'datetimerange',
	daterange : 'daterange',
	monthrange : 'monthrange'
};
