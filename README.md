# vue-element-template

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
### Vue CLI 3 项目的 webpack 配置信息
```
	开发环境：`npx vue-cli-service inspect --mode development`
	生产环境：`npx vue-cli-service inspect --mode production`
	运行命令，将输出导入到 js 文件：
	开发环境：`npx vue-cli-service inspect --mode development >> webpack.config.development.js`
	生产环境：`npx vue-cli-service inspect --mode production >> webpack.config.production.js`
```
### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
