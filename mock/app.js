const Koa = require('koa');
const fs = require('fs');
const path = require('path');
const app = new Koa();
const router = require('koa-router')();
const koaBody = require('koa-body'); //解析上传文件的插件
app.use(koaBody({
	multipart : true,
	formidable : {
		multipart : true,
		maxFileSize : 10 * 1024 * 1024
	}
}));
const static = require('koa-static')

app.use(static(
  path.resolve('./mock')
))

function uploadHandler(file,urls){
	const uploadFile = path.resolve('./mock/upload');
	if (!fs.existsSync(uploadFile)){
		fs.mkdirSync(uploadFile);
	}
	const reader = fs.createReadStream(file.path);
	const ext = file.name.split('.').pop();
	const fileName = `${Math.random().toString()}.${ext}`
	const upStream = fs.createWriteStream(`${uploadFile}/${fileName}`);
	reader.pipe(upStream);
	urls.push(`http://127.0.0.1:7878/upload/${fileName}`)
}

router.post('/api/upload',async (ctx)=>{
	const file = ctx.request.files.file;
	let urls = []
	if(Array.isArray(file)){
		file.map(f=>{
			uploadHandler(f,urls)
		})
	}else{
		uploadHandler(file,urls)
	}


	return ctx.body = {
		succeed : true,
		code : '000000',
		errorMessage : '成功',
		data : {
			urls
		}
	};
});


router.post('/api/login', async (ctx) => {
	ctx.body = {
		succeed : true,
		code : '000000',
		errorMessage : '成功',
		data : {
			username : '张三',
			token : '86c31151c2bb98f6b6e28c30d95fb29e61d68888',
			roles : ['admin']
		}

	};
});

router.post('/api/getUserList', async (ctx) => {
	ctx.body = {
		succeed : true,
		code : '000000',
		errorMessage : '成功',
		data : {
			list : [
				{
					id : '1990021',
					username : '张三',
					mobile : '13567456786',
					gender : 0,
					role : [0],
					state : 0,
					createName : 'admin',
					createTime : '2020-12-12 20:00',
					updateTime : '2020-12-12 20:00'
				},
				{
					id : '1990022',
					username : '李四',
					mobile : '13967456786',
					gender : 1,
					role : [1],
					state : 1,
					createName : 'admin',
					createTime : '2020-12-12 20:00',
					updateTime : '2020-12-12 20:00'
				}
			],
			tableTotal : 2
		}

	};
});

router.post('/api/getRoleList', async (ctx) => {
	ctx.body = {
		succeed : true,
		code : '000000',
		errorMessage : '成功',
		data : {
			list : [
				{
					id : '1990021',
					rolename : '超级管理员',
					roledesc : '拥有所有的权限',
					state : 0,
					createName : 'admin',
					createTime : '2020-12-12 20:00',
					updateTime : '2020-12-12 20:00'
				},
				{
					id : '1990022',
					rolename : '运营管理',
					roledesc : '拥有运营管理权限',
					state : 1,
					createName : 'admin',
					createTime : '2020-12-12 20:00',
					updateTime : '2020-12-12 20:00'
				}
			],
			tableTotal : 2
		}

	};
});

router.post('/api/permissions',async (ctx)=>{
	ctx.body ={
		succeed:true,
		code:'000000',
		errorMessage:'成功',
		data:{
			list:[{
				id : 1,
				type:1,
				label : '系统管理',
				children : [{
					id : 11,
					type:1,
					label : '权限管理',
					children : [
						{
							id : 111,
							type:2,
							label : '按钮新增'
						},
						{
							id : 112,
							type:2,
							label : '按钮修改'
						},
						{
							id : 113,
							type:3,
							label : '用户新增接口'
						}
					]
				},
				{
					id : 12,
					type:1,
					label : '角色管理',
					children : [
						{
							id : 121,
							type:2,
							label : '按钮新增'
						},
						{
							id : 122,
							type:2,
							label : '按钮修改'
						}
					]
				},
				{
					id : 13,
					type:1,
					label : '用户管理',
					children : [
						{
							id : 131,
							type:2,
							label : '按钮新增'
						},
						{
							id : 132,
							type:2,
							label : '按钮修改'
						}
					]
				}]
			}]
		}
	}
})

router.post('/api/getTreeList', async (ctx) => {
	ctx.body = {
		succeed : true,
		code : '000000',
		errorMessage : '成功',
		data : [
			{
				id : 1,
				name : '首页',
				path : '/home',
				parentId : null,
				permission : 'uc:*',
				icon : 'el-icon-s-home',
				sort : 0,
				valid : 1,
				createAt : 1505382641000,
				updateAt : null,
				children : []
			},
			{
				id : 2,
				name : '系统管理',
				path : '/system',
				parentId : null,
				permission : 'system:*',
				icon : 'el-icon-s-tools',
				sort : 3,
				valid : 1,
				createAt : 1505382640000,
				updateAt : 1525659189000,
				children : [
					{
						id : 3,
						name : '权限管理',
						path : 'permissions',
						parentId : 1,
						permission : 'permissions:*',
						icon : 'el-icon-s-flag',
						sort : 1,
						valid : 1,
						createAt : 1505382640000,
						updateAt : 1543800727000,
						children : []
					},
					{
						id : 4,
						name : '角色管理',
						path : 'role',
						parentId : 1,
						permission : 'role:*',
						icon : 'el-icon-s-open',
						sort : 2,
						valid : 1,
						createAt : 1505382640000,
						updateAt : null,
						children : []
					},
					{
						id : 5,
						name : '用户管理',
						path : 'user',
						parentId : 1,
						permission : 'user:*',
						icon : 'el-icon-s-home',
						sort : 3,
						valid : 1,
						createAt : 1505382640000,
						updateAt : null,
						children : []
					}
				]
			}
		]

	};
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(7878, () => {
	console.log('服务已启动端口:7878');
});
